﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simple_arithemic_operation
{
    public partial class Form1 : Form
    {
        int FirstNumber, SecondNumber, Sum;

        public Form1()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e) {
            FirstNumber = int.Parse(textBox1.Text);
            SecondNumber = int.Parse(textBox2.Text);
            Sum = FirstNumber - SecondNumber;
            MessageBox.Show("The Difference between the two numbers is " + Sum.ToString());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FirstNumber = int.Parse(textBox1.Text);
            SecondNumber = int.Parse(textBox2.Text);
            Sum = FirstNumber * SecondNumber;
            MessageBox.Show("The sum of the two numbers is " + Sum.ToString());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FirstNumber = int.Parse(textBox1.Text);
            SecondNumber = int.Parse(textBox2.Text);
            Sum = FirstNumber / SecondNumber;
            MessageBox.Show("The answer after dividing is " + Sum.ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FirstNumber = int.Parse(textBox1.Text);
            SecondNumber = int.Parse(textBox2.Text);
            Sum = FirstNumber + SecondNumber;
            MessageBox.Show("The sum of the two numbers is " + Sum.ToString());
        }
    }
}
